# 如何实现数组函数 reduce

::: tip 更多描述 
 满足以下两个测试用例

``` js
// => 55
reduce([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], (x, y) => x + y)

// => 155
reduce([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], (x, y) => x + y, 100)
``` 
::: 

::: tip Issue 
 欢迎在 Issue 中交流与讨论: [Issue 658](https://github.com/shfshanyue/Daily-Question/issues/658) 
:::

::: tip Author 
回答者: [shfshanyue](https://github.com/shfshanyue) 
:::

``` js
const reduce = (list, fn, ...init) => {
  let next = init.length ? init[0] : list[0]
  for (let i = init.length ? 0 : 1; i < list.length; i++) {
    next = fn(next, list[i], i)
  }
  return next
}
```