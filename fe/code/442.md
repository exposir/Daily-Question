# JS 如何实现一个 sleep/delay 函数

::: tip 更多描述 
 实现一个 sleep 函数格式如下：

``` ts
type sleep = (s: number) => Promise<void>
``` 
::: 

::: tip Issue 
 欢迎在 Issue 中交流与讨论: [Issue 442](https://github.com/shfshanyue/Daily-Question/issues/442) 
:::

::: tip Author 
回答者: [yuuk](https://github.com/yuuk) 
:::

```javascript
function delay(time) {
    return new Promise((resolve)=> {
        setTimeout(() => {
            resolve()
        }, time)
    })
}
```