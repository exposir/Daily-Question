# JS 中如何实现 call



::: tip Issue 
 欢迎在 Issue 中交流与讨论: [Issue 674](https://github.com/shfshanyue/Daily-Question/issues/674) 
:::

::: tip Author 
回答者: [shfshanyue](https://github.com/shfshanyue) 
:::

``` js
const call = (fn, thisObj, ...args) => {
  thisObj.fn = fn;
  const r = thisObj.fn(...args);
  delete thisObj.fn; 
  return r;
}
```